import React, { useState } from 'react';
import TodoForm from './TodoForm';
import TodoList from './TodoList';

export default function Todo({className, theme}) {

    const [todo, setTodo] = useState('');
    const [todoList, setTodoList] = useState([]);

    return (
            <div className={`card ${ className } bg-${theme} text-${theme == 'light' ? 'black' : 'white'}`}>
                <div className="card-header">
                    <h2>Todolist manager</h2>
                </div>
                <div className="card-body">
                    <TodoForm todo={todo} setTodo={setTodo} todoList={todoList} setTodoList={setTodoList} />
                    <TodoList todoList={todoList} setTodoList={setTodoList} />
                </div>
            </div>
    );
}