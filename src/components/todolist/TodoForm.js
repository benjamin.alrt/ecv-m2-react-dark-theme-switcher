import { nanoid } from 'nanoid';
import { useState } from 'react';
import { Form, Button, FormGroup, FormControl } from 'react-bootstrap'

export default function TodoForm({todo, setTodo, todoList, setTodoList}) {

    const handleSubmit = (e) => {
        e.preventDefault();
        setTodoList([...todoList, {name : todo, id : nanoid()}]);
        setTodo('');
    }

    const handleChange = ({ target : { value } }) => {
        setTodo(value);
    }

    return (
        <div className="container">
            <div className="pt-3 d-flex flex-column gap-3">
                <h3 className='m-0 text-start'>Add todo :</h3>
                <Form className="flex-grow-1" onSubmit={handleSubmit}>
                    <FormGroup className="d-flex align-items-center gap-3">
                        <FormControl placeholder='Type your task...' type="text" value={todo} onChange={handleChange} />
                        <Button variant="primary" type="submit">Add</Button>
                    </FormGroup>
                </Form>
            </div>
            <hr />
        </div>
    );
}