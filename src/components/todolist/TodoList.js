import React from 'react';
import TodoListItem from './TodoListItem';

export default function TodoList({todoList, setTodoList}) {

    return (
        <div className='container d-flex flex-column gap-2'>
            <h3 className='text-start'>My todo list :</h3>
            <div className="d-flex flex-column gap-2">
                {todoList.length == 0 &&

                    <p className='text-start'>Your list is empty !</p>
                
                }
                {todoList.map((todoListItem,key) => <TodoListItem key={key} props={{todoListItem, todoList, setTodoList, key}} />)}
            </div>
        </div>   
    );
}