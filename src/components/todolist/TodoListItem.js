import React, { useState } from 'react';
import { Form, Button, FormGroup, FormControl } from 'react-bootstrap'
import TodoForm from './TodoForm';

export default function TodoListItem({ props : { todoList, setTodoList, todoListItem, key }}) {

    const deleteTodoItem = () => {
        setTodoList(todoList.filter(({id}) => id !== todoListItem.id))
    }

    return (
        <div className='d-flex align-self-stretch align-items-center gap-3'>
            <div className='rounded border p-2 flex-grow-1'>
                <p className='text-start m-0'><b>#{key + 1}</b> - {todoListItem.name}</p>
            </div>
            <Button variant="danger" onClick={deleteTodoItem}>Done</Button>
        </div>
    );
}