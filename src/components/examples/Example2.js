import { Component } from "react";

class Example2 extends Component {

    constructor(){
        super()
        this.state = {
            date : new Date()
        };
    }

    componentDidMount() {
        this.timer = setInterval(() => this.tick());
    }
    
    componentWillUnmount() {
        clearInterval(this.timer);
    }

    tick() {
        this.setState({
            date : new Date()
        });
    }

    render() {
        return (
            <div className="pb-2 border-bottom">
                <div className="bg-black">
                    <h1 className="text-light">Exemple 2</h1>
                </div>
                <h2>L'horloge !</h2>
                <p>{ this.state.date.toLocaleTimeString() }</p>
                
            </div>
        )
    }

}

export default Example2;