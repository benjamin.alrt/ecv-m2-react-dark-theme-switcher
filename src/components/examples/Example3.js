import { Component } from "react";
import { Button } from "react-bootstrap";

class Example3 extends Component {

    constructor(){
        super()
        this.state = {
            isToggle : true,
        };
    }

    handleClick() {
        this.setState({isToggle : !this.state.isToggle})
    }

    render() {
        return (
            <div className="pb-2 border-bottom">
                <div className="bg-black">
                    <h1 className="text-light">Exemple 3</h1>
                </div>
                <h2>Le toggle</h2>
                <p><Button variant="primary" onClick={() => this.handleClick()}>{this.state.isToggle ? 'ON' : 'Off'}</Button></p>
            </div>
        )
    }

}

export default Example3;