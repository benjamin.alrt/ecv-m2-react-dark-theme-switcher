import { Component } from "react";
import { Button, Form } from "react-bootstrap";

class Example4 extends Component {

    constructor(){
        super()
        this.state = {
            userName: ''
        };
    }

    handleChange( { target : { value } } ) {
        this.setState({userName : value })
    }

    handleSubmit( event ) {
        event.preventDefault();
        alert(`Bonjour : ${this.state.userName}`)
    }

    render() {
        return (
            <div className="pb-2 border-bottom">
                <div className="bg-black">
                    <h1 className="text-light">Exemple 4</h1>
                </div>
                <h2 class="text-danger">formulaire</h2>
                { this.state.userName }
                <Form onSubmit={(e) => this.handleSubmit(e)}>
                    <Form.Label class="text-success">
                        <h3>Votre nom :</h3>
                    </Form.Label>
                    <Form.Control onChange={(e) => this.handleChange(e)} value={this.state.userName} type="text" />
                    <Button vairant="primary" type="submit">Submit</Button>
                </Form>
            </div>
        )
    }

}

export default Example4;