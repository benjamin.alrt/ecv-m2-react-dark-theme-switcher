import React, { useEffect, useState } from 'react'

export default function ApiExample({className, theme}) {

    const [items, setItems] = useState([]);
    const [apiSuccess, setApiSuccess] = useState(null)
    const [apiError, setApiError] = useState(null)

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/users")
            .then((res) => res.json())
            .then((json) => {
                setApiSuccess(true)
                setItems(json)
            })
            .catch((error) => {
                setApiSuccess(false)
                setApiError(error);
            });
    }, []);

    return (
        <div className={`card ${ className } bg-${theme} text-${theme == 'light' ? 'black' : 'white'}`}>
            <div className="card-header">
                <h2>Api Example</h2>
            </div>
            <div className="card-body">
                <div className='d-flex flex-column gap-2'>
                    <h3 className='text-start'>My users from API :</h3>
                    {apiSuccess === null && 
                        <h1>LOADING.....</h1>
                    }
                    {apiSuccess === true &&
                        <ul className="px-4">
                            {items.map((item, key) => (
                                <li className='text-start' key={key}>{item.name}</li>
                            ))}
                        </ul>
                    }
                    {apiError &&
                        <p className='text-danger'>{apiError.message}</p>
                    }
                </div>
            </div>
        </div>
    )
}
