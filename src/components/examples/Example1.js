import { Button } from "react-bootstrap"
import { Component, useEffect, useState } from "react"

const Example1 = () => {
 
  const [count, setCount] = useState("0%");

  const countOne = (e) => {
    if(e.clientX < 700){
      setCount('100%')
    } else {
      setCount(Math.round((Math.random() * 100)) + '%')
    }
  }

  return (
    <div className="pb-2 border-bottom">
      <div className="bg-black">
        <h1 className="text-light">Exemple 1</h1>
      </div>
      <h2>Compteur</h2>
      <p>{ count }</p>
      <Button onClick={(e) => countOne(e)} variant="primary">Test</Button>
    </div>

  )
}

export default Example1 