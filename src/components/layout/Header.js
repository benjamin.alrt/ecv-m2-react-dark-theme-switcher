import { Navbar, Button } from "react-bootstrap";
import ThemeSwitcher from "../theme/ThemeSwitcher";

export default function Header({theme, setTheme}) {

    return ( 
        <Navbar bg={theme} variant={theme}>
            <div className="container">
                <Navbar.Brand href="#home">
                    Cours React
                </Navbar.Brand>

                <Navbar.Collapse className="justify-content-end">
                    <ThemeSwitcher theme={theme} setTheme={setTheme} />
                </Navbar.Collapse>
            </div>
        </Navbar>
    );

}