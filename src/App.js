import { useState } from 'react';
import './App.css';
import ApiExample from './components/examples/ApiExample';
import Header from './components/layout/Header';
import Todo from './components/todolist/Todo';

function App() {

  const [theme, setTheme] = useState('light')

  const bodyStyle = {
    dark : {
      backgroundColor : 'black',
      transition: 'all 0.5s',
    },
    light : {
      backgroundColor : '#dddddd',
      transition: 'all 0.5s',
    }
  }

  return (
    <div className="App d-flex flex-column" style={{minHeight : '100vh'}}>
      <Header theme={theme} setTheme={setTheme} />
      <div style={bodyStyle[theme]} className={`d-flex flex-column flex-grow-1`}>
        <div className={`container d-flex py-5 flex-grow-1 gap-5 relative`}>
          <Todo theme={theme} className="w-50" />
          <ApiExample theme={theme} className="w-50" />
        </div>
      </div>
    </div>
  );
}

export default App;
